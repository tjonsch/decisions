//
//  ViewController.h
//  decisions
//
//  Created by Tyler Schade on 5/24/14.
//  Copyright (c) 2014 Tyler Schade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *YesOrNo;
@property (weak, nonatomic) IBOutlet UIButton *tapButton;

@end
