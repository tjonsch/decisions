//
//  ViewController.m
//  decisions
//
//  Created by Tyler Schade on 5/24/14.
//  Copyright (c) 2014 Tyler Schade. All rights reserved.
//

#import "ViewController.h"
#import "ynDataController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showAnswer:(int)answer
{
    
}
- (IBAction)tapped:(id)sender
{
    //[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(tgm:) userInfo:nil repeats:NO];
    //[_tapButton setHidden:YES];
    
    ynDataController *answer = [[ynDataController alloc] init];
    int yn = [answer getAnswer];
    //decide
    if (yn == 0) {
        _YesOrNo.text = @"yes";
    }else{
        _YesOrNo.text = @"no";
    }
    
}

@end
