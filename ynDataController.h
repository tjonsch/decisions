//
//  ynDataController.h
//  decisions
//
//  Created by Tyler Schade on 5/24/14.
//  Copyright (c) 2014 Tyler Schade. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ynDataController : NSObject

- (int)getAnswer;

@end
