//
//  ynDataController.m
//  decisions
//
//  Created by Tyler Schade on 5/24/14.
//  Copyright (c) 2014 Tyler Schade. All rights reserved.
//

#import "ynDataController.h"

@implementation ynDataController

- (int)getAnswer
{
    int number = arc4random() % 100;
    int answer = number % 2;
    return answer;
}

@end
